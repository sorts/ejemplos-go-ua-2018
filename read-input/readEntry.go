/*

read input data
---------------
example:
go run readEntry

*/

package main

import (
	"bufio"
	"fmt"
	"os"
	"unicode"
	"unicode/utf8"
)

func main() {

	japstr := "日本語"
	fmt.Printf("%v\n", japstr)

	// # Leer string con formato Scanf %s
	var input string
	fmt.Print("Enter Your Name=")
	fmt.Scanf("%s\n", &input)
	fmt.Println("Hello " + input)

	// conversion string to rune #1
	var str = input
	for len(str) > 0 {
		r, size := utf8.DecodeRuneInString(str)
		fmt.Printf("char %c size %v dec %x\n", r, size, str[0])
		str = str[size:]
	}

	fmt.Scanln() // wait

	// Leer linea (string) sin formato
	fmt.Print("Enter Your Full Name=")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan() // use `for scanner.Scan()` to keep reading
	input = scanner.Text()
	str = input
	// string to rune
	for len(str) > 0 {
		r, size := utf8.DecodeRuneInString(str)
		fmt.Printf("%c %v %x\n", r, size, str[0])
		str = str[size:]
	}

	// tipo char no existe en go, pero existe el tipo byte que es lo mismo ;)
	var character byte
	character = 'A'
	fmt.Printf("\n%c\n", character)

	// string to rune #2
	fmt.Printf("%c", []rune(input))

	// string to rune #3
	for _, r := range input {
		rr := unicode.ToUpper(r) // uppercase conversion
		fmt.Printf("%c", rr)
	}

	return
}
