# Ejemplos Go UA

# README #

Este repositorio contiene ejemplos de código en el lenguaje de Programación GO

### Ejemplos básicos ###

* Lectura de caracteres
* Conversión string a []rune
* Lectura de ficheros
* Arquitectura Cliente/Servidor HTTPS
* Serialización JSON
* Codificación base64
* Cálculo hash SHA512
* Cifrado AES256

### Generación certificados ###

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout key.pem -out cert.pem -config C:\openssl.cnf