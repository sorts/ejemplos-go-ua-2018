package main

/* Ejemplo en Go para leer texto de un fichero */

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	{
		file, err := os.Open("C:/temp/file.txt")
		defer file.Close()
		check(err)

		// conversion de Windows1252 a UTF8, solo utilizar cuando el fichero de entrada tenga esa codificación
		// La transformación solo es necesaria si el fichero de texto tiene la codificación Windows 1252, por ejemplo al crear un fichero en el Bloc de notas.
		dec := transform.NewReader(file, charmap.Windows1252.NewDecoder())

		scanner := bufio.NewScanner(dec)
		for scanner.Scan() {
			fmt.Println(scanner.Text())
		}
	}

	{
		// lectura completa de ficheros (precaucion! copia todo el fichero a memoria)
		file, err := ioutil.ReadFile("C:/temp/file.txt")
		check(err)
		fmt.Println(string(file))
	}

	{
		file, err := os.Open("C:/temp/file.txt")
		check(err)
		defer file.Close()
		b, err := ioutil.ReadAll(file)
		fmt.Println(string(b))
	}

	{
		// Lectura por trozos (bytes)
		file, err := os.Open("C:/temp/file.txt")
		check(err)
		defer file.Close()

		buf := make([]byte, 2)

		for {
			n, err := file.Read(buf)

			if n > 0 {
				fmt.Print(string(buf[:n]))
			}

			if err == io.EOF {
				break
			}
			if err != nil {
				log.Printf("read %d bytes: %v", n, err)
				break
			}
		}
	}
}
